import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkoutprogramsComponent }      from './workoutprograms/workoutprograms.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { WorkoutprogramDetailComponent }  from './workoutprogram-detail/workoutprogram-detail.component';


const routes: Routes = [
  { path: 'detail/:id', component: WorkoutprogramDetailComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'workoutprograms', component: WorkoutprogramsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}