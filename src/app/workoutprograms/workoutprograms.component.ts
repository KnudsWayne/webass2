import { Component, OnInit } from '@angular/core';
import { Workoutprogram } from '../workoutprogram';

import { WorkoutprogramService } from '../workoutprogram.service';

@Component({
  selector: 'app-workoutprograms',
  templateUrl: './workoutprograms.component.html',
  styleUrls: ['./workoutprograms.component.css']
})
export class WorkoutprogramsComponent implements OnInit {
  workoutprograms: Workoutprogram[];

  constructor(private workoutprogramService: WorkoutprogramService) { }

  ngOnInit() {
    this.getWorkoutprograms();
  }

  getWorkoutprograms(): void {
    this.workoutprogramService.getWorkoutprograms()
    .subscribe(workoutprograms => this.workoutprograms = workoutprograms);
  }
}