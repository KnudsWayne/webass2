import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutprogramsComponent } from './workoutprograms.component';

describe('WorkoutprogramsComponent', () => {
  let component: WorkoutprogramsComponent;
  let fixture: ComponentFixture<WorkoutprogramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutprogramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutprogramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
