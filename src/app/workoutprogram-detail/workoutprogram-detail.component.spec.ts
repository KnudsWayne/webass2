import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutprogramDetailComponent } from './workoutprogram-detail.component';

describe('WorkoutprogramDetailComponent', () => {
  let component: WorkoutprogramDetailComponent;
  let fixture: ComponentFixture<WorkoutprogramDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkoutprogramDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutprogramDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
