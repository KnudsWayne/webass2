import { Component, OnInit, Input } from '@angular/core';

import { Workoutprogram } from '../workoutprogram';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { WorkoutprogramService }  from '../workoutprogram.service';

@Component({
  selector: 'app-workoutprogram-detail',
  templateUrl: './workoutprogram-detail.component.html',
  styleUrls: ['./workoutprogram-detail.component.css']
})
export class WorkoutprogramDetailComponent implements OnInit {

  @Input() workoutprogram: Workoutprogram;

  constructor(
    private route: ActivatedRoute,
    private workoutprogramService: WorkoutprogramService,
    private location: Location
  ) {}

  ngOnInit(): void {
  this.getWorkoutprogram();
}

goBack(): void {
  this.location.back();
}

getWorkoutprogram(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  this.workoutprogramService.getWorkoutprogram(id)
    .subscribe(workoutprogram => this.workoutprogram = workoutprogram);
}

}
