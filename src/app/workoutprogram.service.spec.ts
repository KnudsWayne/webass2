import { TestBed } from '@angular/core/testing';

import { WorkoutprogramService } from './workoutprogram.service';

describe('WorkoutprogramService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkoutprogramService = TestBed.get(WorkoutprogramService);
    expect(service).toBeTruthy();
  });
});
