import { Injectable } from '@angular/core';

import { Workoutprogram } from './workoutprogram';
import { WORKOUTPROGRAMS } from './mock-workoutprograms';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class WorkoutprogramService {

  getWorkoutprograms(): Observable<Workoutprogram[]> {
    // TODO: send the message _after_ fetching the workoutprograms
    this.messageService.add('WorkoutprogramService: fetched workoutprograms');
    return of(WORKOUTPROGRAMS);
  }

  getWorkoutprogram(id: number): Observable<Workoutprogram> {
    // TODO: send the message _after_ fetching the workoutprogram
    this.messageService.add(`WorkoutprogramService: fetched workoutprogram id=${id}`);
    return of(WORKOUTPROGRAMS.find(workoutprogram => workoutprogram.id === id));
  }

  constructor(private messageService: MessageService) { }
}
